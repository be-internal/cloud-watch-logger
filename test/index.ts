import * as dotenv from "dotenv"
dotenv.config()

import { CloudWatchLogger } from "../src"

const logger = new CloudWatchLogger({
  key: process.env.CLOUDWATCH_LOGGER_KEY!,
  secret: process.env.CLOUDWATCH_LOGGER_SECRET!,
  region: process.env.CLOUDWATCH_LOGGER_REGION!,
  group: process.env.CLOUDWATCH_LOGGER_GROUP!,
  stream: process.env.CLOUDWATCH_LOGGER_STREAM!,
  retries: +(process.env.CLOUDWATCH_LOGGER_RETRIES || 0),
})

logger.write("test")
