import { createLogger, format, transports } from 'winston'
import WinstonCloudWatch from 'winston-cloudwatch'
import { TransportStreamOptions } from 'winston-transport'

const formatMeta = (meta: any, stringify: boolean = true) => {
  let splat = meta[Symbol.for('splat')]?.filter((d: any) => !d.stack)

  if (splat && splat.length) {
    splat = splat.length === 1 ? splat[0] : splat

    if (!stringify) {
      return splat
    }

    return `\n  Meta: ${JSON.stringify(splat, null, 2)
      .replace(/^[^\[\]\{\}]/gm, '   ')
      .replace(/^[\]]/gm, '  ]')
      .replace(/^[\}]/gm, '  }')}`
  }

  if (!stringify) {
    return undefined
  }

  return ''
}

export class Logger {
  private _logger = createLogger({
    level: 'debug',
    format: format.json(),
    transports: [
      new transports.Console({
        format: format.combine(
          format.errors({ stack: true }),
          format.colorize({
            all: true,
          }),
          format.label({
            label: '[LOGGER]',
          }),
          format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
          }),
          format.printf((info) => {
            const formattedMessage = `${info.label}  ${info.timestamp}  ${info.level}`

            if (info.stack) {
              return `${formattedMessage}${formatMeta(info)}\n  ${info.stack}`
            }

            return `${formattedMessage} : ${info.message}${formatMeta(info)}`
          })
        ),
        level: 'debug',
      }),
    ],
  })

  constructor() {}

  public static create() {
    return new Logger()
  }

  public get logger() {
    return this._logger
  }

  public file(params: {
    level: TransportStreamOptions['level']
    filename: string
    dirname?: string
  }) {
    this._logger.add(
      new transports.File({
        format: format.combine(
          format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
          }),
          format.printf(
            (info) =>
              `${JSON.stringify({
                timestamp: info.timestamp,
                level: info.level,
                message: info.message,
                meta: formatMeta(info, false),
              })}`
          )
        ),
        level: params.level,
        filename: `${params.filename}.log`,
        dirname: params.dirname,
      })
    )

    return this
  }

  public fileError(params: { filename: string; dirname?: string }) {
    this._logger.add(
      new transports.File({
        format: format.combine(
          format.errors({ stack: true }),
          format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
          }),
          format.printf(
            (info) =>
              `${JSON.stringify({
                timestamp: info.timestamp,
                message: info.message,
                stack: info.stack,
                meta: formatMeta(info, false),
              })}`
          )
        ),
        level: 'error',
        filename: `${params.filename}.log`,
        dirname: params.dirname,
      })
    )

    return this
  }

  public cloudWatch(params: {
    level: TransportStreamOptions['level']
    accessKeyId: string
    secretAccessKey: string
    region: string
    logGroupName: string
    logStreamName: string
  }) {
    this._logger.add(
      new WinstonCloudWatch({
        awsOptions: {
          credentials: {
            accessKeyId: params.accessKeyId,
            secretAccessKey: params.secretAccessKey,
          },
        },
        messageFormatter: (info) =>
          `${JSON.stringify({
            message: info.message,
            stack: info.stack,
            meta: formatMeta(info, false),
          })}`,
        level: params.level,
        logGroupName: params.logGroupName,
        logStreamName: params.logStreamName,
        awsRegion: params.region,
      })
    )

    return this
  }
}
