# CloudWatch Logger

## Usages

### AdonisJS

```js
import { Logger } from "@bemobile-tech/cloud-watch-logger"

const logger = Logger.create()
  .file({
    level: 'verbose',
    filename: 'test',
    dirname: 'logs',
  })
  .fileError({
    filename: 'test.errors',
    dirname: 'logs',
  })
  .cloudWatch({
    level: 'info',
    accessKeyId: process.env.CLOUDWATCH_LOGGER_KEY!,
    secretAccessKey: process.env.CLOUDWATCH_LOGGER_SECRET!,
    region: process.env.CLOUDWATCH_LOGGER_REGION!,
    logGroupName: process.env.CLOUDWATCH_LOGGER_GROUP!,
    logStreamName: process.env.CLOUDWATCH_LOGGER_STREAM!,
  }).logger

export const logger: LoggerConfig = {
  prettyPrint: Env.get("NODE_ENV") === "development",
  stream:
    Env.get("NODE_ENV") === "development"
      ? undefined
      : logger,
}
```

### JavaScript/TypeScript

```js
import { Logger } from "@bemobile-tech/cloud-watch-logger"

const logger = Logger.create()
  .file({
    level: 'verbose',
    filename: 'test',
    dirname: 'logs',
  })
  .fileError({
    filename: 'test.errors',
    dirname: 'logs',
  })
  .cloudWatch({
    level: 'info',
    accessKeyId: process.env.CLOUDWATCH_LOGGER_KEY!,
    secretAccessKey: process.env.CLOUDWATCH_LOGGER_SECRET!,
    region: process.env.CLOUDWATCH_LOGGER_REGION!,
    logGroupName: process.env.CLOUDWATCH_LOGGER_GROUP!,
    logStreamName: process.env.CLOUDWATCH_LOGGER_STREAM!,
  }).logger

try {
  throw new Error('isso é um erro')
} catch (error) {
  logger.error('teste', error)
}
logger.warn('warn')
logger.info('info')
logger.http('http')
logger.verbose('verbose')
logger.debug('debug')
```
